var mongoose = require('mongoose');
var studentSchema = mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true
    },
    address: String,
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export student model
var Student = module.exports = mongoose.model('student', studentSchema);
module.exports.get = function (callback, limit) {
    Student.find(callback).limit(limit);
}